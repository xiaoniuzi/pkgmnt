import base64
import requests

org_url = "https://gitee.com/api/v5/orgs"
repo_url = "https://gitee.com/api/v5/repos"
org = "testPatchTrack"
token = "7879f21fccc122380d1d3aadab6e2607"


def get_repo(org_url, org, token):
    '''
    获取指定组织下的所有仓库
    :param org_url:
    :param org:
    :param token:
    :return:
    '''
    url = '/'.join([org_url, org, 'repos'])
    param = {'access_token': token}
    all_repo = list()

    r = requests.get(url, params=param)
    for item in r.json():
        repo_name = item['full_name']
        all_repo.append(repo_name)
    return all_repo


def get_branch(repo_url, repo, token):
    '''
    获取指定仓库的所有分支
    :param repo_url:
    :param repos:
    :param token:
    :return:
    '''
    param = {'access_token': token}
    branch_list = list()
    url = '/'.join([repo_url, repo, 'branches'])

    r = requests.get(url, params=param)
    for item in r.json():
        branch_list.append(item['name'])
    return branch_list


def get_yaml_content(repo_url, repo, branch, token):
    '''
    获取指定仓库，指定分支的yaml文件内容
    :param repo_url:
    :param repo:
    :param branch:
    :param token:
    :return:
    '''
    yaml_content_dict = dict()
    path = repo.split('/')[1] + '.yaml'
    url = '/'.join([repo_url, repo, 'contents', path])
    param = {'access_token': token, 'ref': branch}
    r = requests.get(url, params=param).json()
    content = str(base64.b64decode(r['content']), encoding='utf-8')
    for item in content.split('\n'):
        if item:
            key = item.split(':')[0].strip(' ')
            value = item.split(':')[1].strip(' ')
            yaml_content_dict[key] = value
    return yaml_content_dict


if __name__ == '__main__':
    all_repo_list = get_repo(org_url, org, token)

    for repo in all_repo_list:
        branch_list = get_branch(repo_url, repo, token)
        for branch in branch_list:
            yaml_content_dict = get_yaml_content(repo_url, repo, branch, token)
            print(repo, branch, yaml_content_dict)

