import logging

from flask import request
from flask_restx import Resource
from api.business import create_tracking, update_tracking
from api.serializers import tracking, page_of_tracking
from api.parsers import pagination_arguments
from api.restplus import api
from database.models import Tracking

log = logging.getLogger(__name__)

ns = api.namespace('tracking', description='Operations related to tracking')


@ns.route('')
class TrackingCollection(Resource):

    @api.expect(pagination_arguments)
    @api.marshal_with(page_of_tracking)
    def get(self):
        """
        Returns list of tracking.
        """
        args = pagination_arguments.parse_args(request)
        page = args.get('page', 1)
        per_page = args.get('per_page', 10)

        tracking_query = Tracking.query
        tracking_page = tracking_query.paginate(page, per_page, error_out=False)

        return tracking_page

    @api.expect(tracking)
    def post(self):
        """
        Creates a new tracking.
        """
        create_tracking(request.json)
        return None, 201

    @api.expect(tracking)
    @api.response(204, 'Tracking successfully updated.')
    def put(self):
        """
        Updates a tracking.

        Use this method to change the tracking.

        * Send a JSON object with the new data in the request body.

        ```
        {
          "scm_repo": "New SCM repo",
          "scm_branch": "New SCM branch",
          ...
        }
        ```

        """
        data = request.json
        update_tracking(data)
        return None, 204