import logging

from flask import request
from flask_restx import Resource
from api.business import create_issue, update_issue
from api.serializers import issue
from api.restplus import api
from database.models import Issue

log = logging.getLogger(__name__)

ns = api.namespace('issue', description='Operations related to issue')


@ns.route('')
class IssueCollection(Resource):

    @api.marshal_list_with(issue)
    def get(self):
        """
        Returns list of issue.
        """
        categories = Issue.query.all()
        return categories

    @api.response(201, 'Issue successfully created.')
    @api.expect(issue)
    def post(self):
        """
        Creates a new issue.
        """
        data = request.json
        create_issue(data)
        return None, 201

    @api.expect(issue)
    @api.response(204, 'Category successfully updated.')
    def put(self):
        """
        Updates a issue.

        Use this method to change the data of issue.

        * Send a JSON object with the new data in the request body.

        ```
        {
          "issue": "issue"，
          "tracking_id": "New Tracking ID"
        }
        ```

        * Specify the ID of the category to modify in the request URL path.
        """
        data = request.json
        update_issue(data)
        return None, 204