from . import Config


class DevelopementConfig(Config):
    '''
        @说明    开发模式下的配置
        @时间    2020-05-11 20:59:39
        @作者    gzt
    '''

    DEBUG = True
