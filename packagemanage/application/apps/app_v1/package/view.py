from flask import request
from flask_restful import Resource


class Packages(Resource):
    '''
    Description: interface for package info management
    Restful API: get
    changeLog:
    '''

    def get(self, *args, **kwargs):
        '''
        Description: Get all package info from a database
        input:
            dbName
        return:
            Package[
            sourceName : 
            version :
            release :
            epoch :
            license :
            maintainer :
            maintainLevel :
            sourcrURL :
            downloadURL :
            dbName :
            buildDep :
            subpack :[
                binaryName :[
                    installDep :
                    arch : arch or noarch
                    ]
                ]
            ]
        Exception:
        Changelog：
        '''
        return 'Get all packages info'

class SinglePack(Resource):
    '''
    description: single package management
    Restful API: get、put
    ChangeLog:
    '''
    def get(self, *args, **kwargs):
        '''
        description: Searching a package info
        input:
            sourceName
            dbName
            version(option)
        return:
            Package[
            sourceName,
            version,
            release,
            epoch,
            license,
            maintainer,
            maintainLevel,
            sourcrURL,
            downloadURL,
            dbName,
            buildDep,
            subpack[
                binaryName[
                    installDep,
                    arch,
                    ]
                ]
            ]
        exception:
        changeLog:           
        '''
        return 'query specific pacakge info'

    def put(self, *args, **kwargs):
        '''
        Description: update a package info
        input:
            packageName
            dbName
            maintainer
            sourceURL
            maintainLevel
        return:
        exception:
        changeLog:
        '''
        return 'Update package info'


class InstallDepend(Resource):
    '''
    Description: install depend of package
    Restful API: post
    changeLog:
    '''
    def post(self, *args, **kwargs):
        '''
        Description: Query a package's install depend(support querying in one or more databases)
        input:
            binaryName ：
            type ：srouce/binary
            version : option,default is the lastest
            dbPreority：the array for database preority
        return:
            resultList[
                result[
                    binaryName: binary package name
                    srcName: the source package name for that binary packge
                    dbName: 
                    type: install  install or build, which depend on the function
                                    #安装依赖或编译依赖（在此函数中统一为安装依赖，即返回 install）,
                    parentNode: the binary package name which is the install depend for binaryName
                                #父节点二进制包名，即binaryName是parentNode的安装依赖
                ]
            ]
        exception:
        changeLog:
        '''
        return 'query specific pacakge install depend'


class BuildDepend(Resource):
    """docstring for SinglePack"""
    def post(self, *args, **kwargs):
        '''
        Description: Query a package's build depend(support querying in one or more databases)
        input:
            sourceName ：
            selfBuild ：
                        if 0, the function used for find build depend package and build depend package name's install depend
                        if 1, the function used for fing build depend package 
                        #若为0，则返回该源码包的编译依赖，以及编译依赖的安装依赖
                        #若为1，则返回该源码包的编译依赖，以及编译依赖的源码包的编译依赖
            version : option,default is the lastest
            dbPreority：the array for database preority
        return:
            resultList[
                restult[
                    binaryName:
                    srcName: 
                    dbName: 
                    type: build,  install or build, which depend on the function
                        # 安装依赖或编译依赖（在此函数中都有可能，即编译依赖或编译依赖的安装依赖）
                    parentNode: the binary package name which is the build/install depend for binaryName
                                #父节点包名，若type是build，则binaryName是parentNode的编译依赖，若type是install，则
                                binaryName是parentNode的安装依赖
                ]
            ]
        exception:
        changeLog:
        '''
        return 'query specific pacakge build depend'

class SelfDepend(Resource):
    """docstring for SinglePack"""
    def post(self, *args, **kwargs):
        '''
        description: Query a package's all dependencies including install and build depend
                        (support quering a binary or source package in one or more databases)
        input:
            packageName:
            packageType: source/binary
            selfBuild :0/1
            withSubpack: 0/1
            version : option,default is the lastest
            dbPreority：the array for database preority
        return:
            resultList[
                restult[
                    binaryName:
                    srcName: 
                    dbName: 
                    type: install or build, which depend on the function
                        # 安装依赖或编译依赖（在此函数中都有可能，即编译依赖或编译依赖的安装依赖）
                    parentNode: the binary package name which is the build/install depend for binaryName
                                #父节点包名，若type是build，则binaryName是parentNode的编译依赖，若type是install，则
                                 binaryName是parentNode的安装依赖
                ]
            ]
               
        exception:
        changeLog:
        '''
        return 'query specific pacakge self depend'


class BeDepend(Resource):
    """docstring for SinglePack"""
    def post(self, *args, **kwargs):
        '''
        description: get all database
        input:
            packageName:
            withSubpack: 0/1
            version: (option)
        return:
            resultList[
                restult[
                    binaryName:
                    srcName: 
                    dbName: 
                    type: beinstall or bebuild, which depend on the function
                    childNode: the binary package name which is the be built/installed depend for binaryName
                                #子节点包名，若type是build，则childNode是binaryName的编译依赖，若type是install，则
                                childNode是binaryName的安装依赖
                ]
            ]
        exception:
        changeLog:
        '''
        return 'query specific pacakge bedpend'


class Repodatas(Resource):
    """docstring for SinglePack"""
    def get(self, *args, **kwargs):
        '''
        description: get all database
        input:
        return:
            databasesName
            status
            priority
        exception:
        changeLog:
        '''
        return 'Get all imported databases'

    def post(self, *args, **kwargs):
        return 'Import new databases'

    def put(self, *args, **kwargs):
        return 'Update database'










