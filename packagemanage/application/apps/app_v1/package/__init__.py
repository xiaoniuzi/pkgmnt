from flask.blueprints import Blueprint
from flask_restful import Api
from application.apps.app_v1.package.url import urls


package = Blueprint('package', __name__)

# 初始化restapi
api = Api()

for view, url in urls:
    api.add_resource(view, url)


__all__ = ['package', 'api']
