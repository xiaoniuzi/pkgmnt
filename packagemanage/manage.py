from application import init_app
from flask_script import Manager

app = init_app('dev')
manager = Manager(app)

if __name__ == "__main__":
    manager.run()