
# 一、背景：

在 openEuler 发行版开发过程，需要及时更新上游社区各个软件包的最新代码，修改功能 bug 及安全问题，确保发布的 openEuler 发行版尽可能避免缺陷和漏洞。


# 二、原始需求 -- 社区补丁跟踪

对软件包管理系统的软件，主动监控上游社区提交，自动生成补丁，并自动提交 issue 给对应的 maintainer，同时自动验证补丁基础功能，减少验证工作量支持 maintainer 快速决策。


# 三、依赖组件

- 开源软件的上游 SCM（包括 Github、Gitlab、SVN）
- 存放包源码的 Gitee Git 仓库


# 四、License

Mulan V2


# 五、功能概述

Gitee 的 src-openEuler 组织下存放软件包的包源码，每个软件包存放在一个 Git 仓库，其中包含构建软件包 source RPMs 以及进一步构建 binary RPMs 所需的文件。
提供接口，允许用户配置所需跟踪的软件及版本，以及对应跟踪的上游社区 SCM 信息（包括仓库、分支、最新处理过的 Commit），当上游源代码有变更时，能够提取这些变更，形成补丁文件，并且在包源码的仓库创建 issue，由 Maintainer 决策是否合并到包源码仓库中，从而能够构建最新的 source RPMs 和 binary RPMs。


# 六、功能设计

- 支持svn/git代码监控
- 自动从svn/git中获取补丁
- 自动提交issue到对应项目
- 自动验证补丁编译和基础测试，并通过讨论的方式回填对应的issue
- 自动过滤已经提交issue的清单
*1.如果有社区有lts分支，直接跟踪LTS分支。2.如果没有LTS,主干合入比较少，可以直接跟踪，3.如果主干合入很多的情况下，建议跟踪fedora/centos/suse*


## 6.1 软件包 Gitee 仓库 xxx.yaml 文件字段

```
version_control: github
src_repo: MirBSD/mksh
src_branch: 1.x
patch_tracking_enabled: true
```


## 6.2 补丁跟踪外部接口

- 查询补丁跟踪项

![](TrackingAPIs.png)

- 查询补丁跟踪 Issue

![](IssueAPIs.png)


## 6.3 初始化及周期性加载流程

![](Load.jpg)


## 6.4 补丁跟踪核心流程

![](PatchTracking.jpg)


## 6.5 Maintainer 补丁处理流程

![](Maintainer.jpg)


# 七、数据表设计

- tracking

| 序号 | 名称 | 说明 | 类型 | 键 | 允许空 | 默认值 |
|    - |   - |    - |   - |  - | - | -  |
| 1 | id | 自增补丁跟踪项序号 | Int | Primary | NO | -  |
| 4 | scmRepo | 软件包上游 SCM 仓库地址 | String | | NO | -  |
| 5 | scmBranch | 上游 SCM 跟踪分支 | String | | NO | -  |
| 6 | scmCommit | 上游代码最新处理过的 CommitID | String | | NO | -  |
| 7 | repo | 包源码在 Gitee 的仓库地址 | String | Primary | NO | -  |
| 8 | branch | 提交 PR 合入的目标分支 | String | Primary | NO | -  |
| 9 | enabled | 是否启动跟踪 | Bool | | NO | -  |

- issue

| 序号 | 名称 | 说明 | 类型 | 键 | 允许空 | 默认值 |
|    - |   - |    - |   - |  - | - | -  |
| 1 | issue |   - |   - |  - | - | -  |
| 2 | trackingID |  - |   - |  - | - | -  |



# 八、配置文件

```
organization: src-openEuler                # Gitee 存放软件包的组织名
token: 349fca66defa55f2605957f7cc3e1234    # 访问 Gitee API 的 token
db_server: 1.2.3.4:5000                    # 数据库 URL
db_user: user                              # 数据库用户名
db_password: password                      # 数据库口令
period: 12h                                # 加载软件包配置文件 xxx.yaml 并检查上游社区源代码变更的周期
```


# 九、实现

- 开发语言：Python
- 数据库：MySQL，优先考虑使用华为云服务
- 运行环境：Docker 方式运行，优先考虑托管在华为云 CCE 或者 CSE
- 日志：使用通用日志框架，日志信息打屏，由 Docker 引擎或者日志服务保存日志


# 十、遗留问题

- SVN、Git SCM 仓库对应的 API 需要验证

